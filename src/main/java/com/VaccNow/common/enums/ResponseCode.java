package com.VaccNow.common.enums;


import org.springframework.util.StringUtils;

public enum ResponseCode {
    SUCCESS(200, "Success"),
    UNAUTHORIZED(401, "Unauthorized"),
    FORBIDDEN(403, "Forbidden"),
    EMPTY_REQUEST(400, "Request cannot be empty"),
    NOT_FOUND(400, "Schedule not found");



    private final Integer code;
    private String message;

    public static ResponseCode format(ResponseCode code, String message) {
        if (code != null) {
            code.message = String.format(code.message, StringUtils.isEmpty(message) ? "Empty message msg" : message);
        }

        return code;
    }

    public Integer getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    private ResponseCode(final Integer code, final String message) {
        this.code = code;
        this.message = message;
    }
}

