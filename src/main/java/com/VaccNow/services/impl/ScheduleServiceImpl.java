package com.VaccNow.services.impl;

import com.VaccNow.common.dto.exception.BaseException;
import com.VaccNow.common.dto.request.ScheduleRequest;
import com.VaccNow.common.dto.response.BaseApiResponse;
import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.ScheduleResponse;
import com.VaccNow.common.enums.ResponseCode;
import com.VaccNow.common.mapper.ScheduleMapper;
import com.VaccNow.domains.dao.ScheduleDao;
import com.VaccNow.domains.entity.Branch;
import com.VaccNow.domains.entity.Schedule;
import com.VaccNow.services.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleDao scheduleDao;

    @Override
    public BasePaginationApiResponse<List<ScheduleResponse>> getAll(int offset, int limit, String sortBy, String sortOrder) {
        Page<Schedule> page = scheduleDao.findAll(PageRequest.of(offset, limit, Sort.by(Sort.Direction.valueOf(sortOrder), sortBy)));
        List<ScheduleResponse> responseList = getResponseList(page.getContent());
        return BasePaginationApiResponse.of(responseList, page.getTotalElements(), page.hasNext());
    }

    @Override
    public ScheduleResponse save(ScheduleRequest request) {
        Schedule entity = ScheduleMapper.requestToEntity(request);
        if (entity == null) {
            throw new BaseException(ResponseCode.EMPTY_REQUEST);
        }
        return getResponse(scheduleDao.save(entity));
    }

    @Override
    public ScheduleResponse applyVaccine(Long scheduleId) {
        Schedule schedule = getByIdOrThrow(scheduleId);
        schedule.setApplied(true);
        schedule.setAppliedOn(LocalDateTime.now());
        return getResponse(scheduleDao.save(schedule));
    }

    @Override
    public BasePaginationApiResponse<List<ScheduleResponse>> getAllAppliedSchedule(int offset, int limit, String sortBy, String sortOrder) {
        Page<Schedule> page = scheduleDao.findAllByIsApplied(PageRequest.of(offset, limit, Sort.by(Sort.Direction.valueOf(sortOrder), sortBy)),true);
        List<ScheduleResponse> responseList = getResponseList(page.getContent());
        return BasePaginationApiResponse.of(responseList, page.getTotalElements(), page.hasNext());
    }

    @Override
    public BasePaginationApiResponse<List<ScheduleResponse>> getAllAppliedScheduleByBranch(int offset, int limit, String sortBy, String sortOrder,Long branchId) {
        Page<Schedule> page = scheduleDao.findAllByBranchId(PageRequest.of(offset, limit, Sort.by(Sort.Direction.valueOf(sortOrder), sortBy)), branchId);
        List<ScheduleResponse> responseList = getResponseList(page.getContent());
        return BasePaginationApiResponse.of(responseList, page.getTotalElements(), page.hasNext());

    }

    @Override
    public BasePaginationApiResponse<List<ScheduleResponse>> getAppliedForDay(int offset, int limit, String sortBy, String sortOrder) {
        Page<Schedule> page = scheduleDao.findByAppliedOnGreaterThanEqualAndAppliedOnLessThanEqual(PageRequest.of(offset, limit, Sort.by(Sort.Direction.valueOf(sortOrder), sortBy)), LocalDateTime.now().minusDays(1), LocalDateTime.now());
        List<ScheduleResponse> responseList = getResponseList(page.getContent());
        return BasePaginationApiResponse.of(responseList, page.getTotalElements(), page.hasNext());
    }

    @Override
    public BasePaginationApiResponse<List<ScheduleResponse>> getAppliedByPeriod(Integer offset, Integer limit, String sortBy, String sortOrder, LocalDateTime from, LocalDateTime to) {
        Page<Schedule> page = scheduleDao.findByAppliedOnGreaterThanEqualAndAppliedOnLessThanEqual(PageRequest.of(offset, limit, Sort.by(Sort.Direction.valueOf(sortOrder), sortBy)), from, to);
        List<ScheduleResponse> responseList = getResponseList(page.getContent());
        return BasePaginationApiResponse.of(responseList, page.getTotalElements(), page.hasNext());
    }

    private List<ScheduleResponse> getResponseList(List<Schedule> entityList) {
        if (CollectionUtils.isEmpty(entityList)) {
            return new ArrayList<>();
        }
        return entityList.stream().map(entity -> getResponse(entity)).collect(Collectors.toList());
    }

    private ScheduleResponse getResponse(Schedule entity) {
        ScheduleResponse response = ScheduleMapper.entityToResponse(entity);
        if (entity == null) {
            return null;
        }
        return response;
    }

    private Schedule getByIdOrThrow(Long id) {
        return scheduleDao.findById(id).orElseThrow(() -> new BaseException(ResponseCode.NOT_FOUND));
    }
}
