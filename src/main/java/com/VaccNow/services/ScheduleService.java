package com.VaccNow.services;

import com.VaccNow.common.dto.request.ScheduleRequest;
import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.ScheduleResponse;

import java.time.LocalDateTime;
import java.util.List;

public interface ScheduleService {
    BasePaginationApiResponse<List<ScheduleResponse>> getAll(int offset, int limit, String sortBy, String sortOrder);
    ScheduleResponse save(ScheduleRequest schedule);
    ScheduleResponse applyVaccine(Long scheduleId);
    BasePaginationApiResponse<List<ScheduleResponse>> getAllAppliedSchedule(int offset, int limit, String sortBy, String sortOrder);
    BasePaginationApiResponse<List<ScheduleResponse>> getAllAppliedScheduleByBranch(int offset, int limit, String sortBy, String sortOrder,Long branchId);
    BasePaginationApiResponse<List<ScheduleResponse>> getAppliedForDay(int offset, int limit, String sortBy, String sortOrder);
    BasePaginationApiResponse<List<ScheduleResponse>> getAppliedByPeriod(Integer offset, Integer limit, String sortBy, String sortOrder, LocalDateTime from, LocalDateTime to);
}
