package com.VaccNow.common.dto.response;

import com.VaccNow.common.pojo.BasePojo;
import com.VaccNow.common.pojo.BranchPojo;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class BranchResponse extends BranchPojo {

}
