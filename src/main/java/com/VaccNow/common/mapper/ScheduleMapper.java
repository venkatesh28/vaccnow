package com.VaccNow.common.mapper;

import com.VaccNow.common.dto.request.ScheduleRequest;
import com.VaccNow.common.dto.response.ScheduleResponse;
import com.VaccNow.domains.entity.Schedule;

public class ScheduleMapper extends BaseMapper{

    public static ScheduleResponse entityToResponse(Schedule entity) {
        if (entity == null) {
            return null;
        }
        ScheduleResponse response = new ScheduleResponse();
        response.setAllocationId(entity.getAllocation().getId());
        response.setApplied(entity.isApplied());
        response.setEmail(entity.getEmail());
        response.setAppliedOn(entity.getAppliedOn());
        response.setBranchId(entity.getBranch().getId());
        response.setSlotId(entity.getSlot().getId());
        response.setPaymentType(entity.getPaymentType());
        setBaseResponse(response,entity);
        return response;
    }

    public static Schedule requestToEntity(ScheduleRequest request) {
        if (request == null) {
            return null;
        }
        Schedule entity = new Schedule();
        setBaseEntity(entity);
        return entity;
    }
}
