package com.VaccNow.controllers;

import com.VaccNow.common.constants.MappingConstants;
import com.VaccNow.common.dto.response.AllocationResponse;
import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.VaccineResponse;
import com.VaccNow.services.AllocationService;
import com.VaccNow.services.VaccineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = (MappingConstants.AllocationConstants.API_BASE))
public class AllocationController {

    @Autowired
    private AllocationService allocationService;

    @GetMapping
    public BasePaginationApiResponse<List<AllocationResponse>> getAll(
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_OFFSET) Integer offset,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_LIMIT) Integer limit,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_BY) String sortBy,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_ORDER) String sortOrder) {

        return allocationService.getAll(offset,limit,sortBy,sortOrder);

    }

    @GetMapping(path = MappingConstants.PathVariables.ID_PARAM)
    public BasePaginationApiResponse<List<AllocationResponse>> getAll(
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_OFFSET) Integer offset,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_LIMIT) Integer limit,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_BY) String sortBy,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_ORDER) String sortOrder,
            @PathVariable Long branchId) {

        return allocationService.getAll(offset,limit,sortBy,sortOrder, branchId);

    }
}
