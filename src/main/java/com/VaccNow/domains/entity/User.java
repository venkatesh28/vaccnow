package com.VaccNow.domains.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = User.Columns.TABLE)
public class User extends Base {

	public interface Columns extends Base.Columns {
		String TABLE = "user";
		String NAME = "name";
		String EMAIL = "email";
	}

	@Column(name = Columns.NAME)
	private String name;

	@Column(name = Columns.EMAIL)
	private String email;


}
