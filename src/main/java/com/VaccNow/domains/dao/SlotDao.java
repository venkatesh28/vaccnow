package com.VaccNow.domains.dao;

import org.springframework.data.domain.Pageable;
import com.VaccNow.domains.entity.Slot;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface SlotDao extends JpaRepository<Slot,Long> {
    Page<Slot> findAllByBranchIdAndStartAtAfter(Pageable of,Long branchId, LocalDateTime time);

}
