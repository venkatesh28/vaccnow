package com.VaccNow.common.dto.response;

import com.VaccNow.common.pojo.SlotPojo;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class SlotResponse extends SlotPojo {
}
