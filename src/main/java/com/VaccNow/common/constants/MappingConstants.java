package com.VaccNow.common.constants;


import com.VaccNow.domains.entity.Base;

import static com.VaccNow.common.constants.MappingConstants.RequestParams.BRANCH_ID;

public interface MappingConstants {
    String BASE = "/vaccnow";

    interface RequestParams{
        String ID = "id";
        String SCHEDULE_ID = "scheduleId";
        String BRANCH_ID = "branchId";

    }

    public interface PathVariables {
        String ID_PARAM = "/{" + RequestParams.ID + "}";
        String SCHEDULE_ID = "/{" + RequestParams.SCHEDULE_ID + "}";
    }

    interface PaginationConstants{
        String DEFAULT_LIMIT = "10";
        String DEFAULT_OFFSET = "0";
        String DEFAULT_SORT_BY = Base.Columns.CREATED_AT;
        String DEFAULT_SORT_ORDER = "DESC";
    }

    interface BranchConstants{
        String API_BASE = BASE+"/branch";
        String AVAILABLE_SLOTS = "/availableSlots/{"+BRANCH_ID+"}";
    }

    interface VaccineConstants{
        String API_BASE = BASE+"/vaccine";
    }

    interface SchedulerConstants{
        String API_BASE = BASE+"/schedule";
        String APPLY = "/applyVaccine"+PathVariables.SCHEDULE_ID;
        String APPLIED = "/applied";
        String APPLIED_PER_DAY = "/applied/perDay";
        String APPLIED_PERIOD = "/applied/period";
        String APPLIED_BRANCH = "/applied/{"+BRANCH_ID+"}";
    }

    interface AllocationConstants{
        String API_BASE = BASE+"/allocation";

    }

}
