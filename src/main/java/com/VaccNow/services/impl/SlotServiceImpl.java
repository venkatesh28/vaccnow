package com.VaccNow.services.impl;

import com.VaccNow.common.dto.response.BaseApiResponse;
import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.BranchResponse;
import com.VaccNow.common.dto.response.SlotResponse;
import com.VaccNow.common.mapper.SlotMapper;
import com.VaccNow.domains.dao.SlotDao;
import com.VaccNow.domains.entity.Slot;
import com.VaccNow.services.SlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class SlotServiceImpl implements SlotService {

    @Autowired
    private SlotDao slotDao;

    @Override
    public BasePaginationApiResponse<List<SlotResponse>> getAvailableSlots(int offset,
                                                           int limit,
                                                           String sortBy,
                                                           String sortOrder,
                                                           Long branchId) {
        Page<Slot> page = slotDao.findAllByBranchIdAndStartAtAfter(PageRequest
                        .of(offset, limit, Sort.by(Sort.Direction.valueOf(sortOrder), sortBy)),
                branchId, LocalDateTime.now());

        List<SlotResponse> responseList = getResponseList(page.getContent());
        return BasePaginationApiResponse.of(responseList, page.getTotalElements(), page.hasNext());
    }

    private List<SlotResponse> getResponseList(List<Slot> entityList) {
        if (CollectionUtils.isEmpty(entityList)) {
            return new ArrayList<>();
        }
        return entityList.stream().map(entity -> getResponse(entity)).collect(Collectors.toList());
    }

    private SlotResponse getResponse(Slot entity) {
        SlotResponse response = SlotMapper.entityToResponse(entity);
        if (entity == null) {
            return null;
        }
        return response;
    }
}
