package com.VaccNow.services;

import com.VaccNow.common.dto.response.BaseApiResponse;
import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.BranchResponse;

import java.util.List;

public interface BranchService {

    BasePaginationApiResponse<List<BranchResponse>> getAll(int offset, int limit, String sortBy, String sortOrder);

}
