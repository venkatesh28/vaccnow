package com.VaccNow.domains.dao;

import com.VaccNow.domains.entity.Vaccine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VaccineDao extends JpaRepository<Vaccine,Long> {
    //Page<Vaccine> findAllByBranchId(Pageable of, Long branchId);
}
