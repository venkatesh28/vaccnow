package com.VaccNow.controllers;

import com.VaccNow.common.constants.MappingConstants;
import com.VaccNow.common.dto.request.ScheduleRequest;
import com.VaccNow.common.dto.response.BaseApiResponse;
import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.ScheduleResponse;
import com.VaccNow.services.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(value = (MappingConstants.SchedulerConstants.API_BASE))
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    public BasePaginationApiResponse<List<ScheduleResponse>> getAll(
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_OFFSET) Integer offset,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_LIMIT) Integer limit,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_BY) String sortBy,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_ORDER) String sortOrder) {

        return scheduleService.getAll(offset,limit,sortBy,sortOrder);
    }

    @PostMapping
    public BaseApiResponse<ScheduleResponse> save(@Valid @RequestBody ScheduleRequest schedule) {
        return BaseApiResponse.data(scheduleService.save(schedule));
    }

    @PutMapping(path = MappingConstants.SchedulerConstants.APPLY)
    public BaseApiResponse<ScheduleResponse> applyVaccine(@PathVariable Long scheduleId) {
        return BaseApiResponse.data(scheduleService.applyVaccine(scheduleId));
    }

    @GetMapping(path = MappingConstants.SchedulerConstants.APPLIED)
    public BasePaginationApiResponse<List<ScheduleResponse>> getAllAppliedSchedule(@RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_OFFSET) Integer offset,
                                                                   @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_LIMIT) Integer limit,
                                                                   @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_BY) String sortBy,
                                                                   @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_ORDER) String sortOrder) {
        return scheduleService.getAllAppliedSchedule(offset,limit,sortBy,sortOrder);
    }

    @GetMapping(path = MappingConstants.SchedulerConstants.APPLIED_BRANCH)
    public BasePaginationApiResponse<List<ScheduleResponse>> getAllAppliedScheduleByBranch(@RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_OFFSET) Integer offset,
                                                                                           @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_LIMIT) Integer limit,
                                                                                           @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_BY) String sortBy,
                                                                                           @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_ORDER) String sortOrder,
                                                                                           @PathVariable Long branchId) {

        return scheduleService.getAllAppliedScheduleByBranch(offset,limit,sortBy,sortOrder,branchId);
    }

    @GetMapping(path = MappingConstants.SchedulerConstants.APPLIED_PER_DAY)
    public BasePaginationApiResponse<List<ScheduleResponse>> getAllAppliedPerDay(@RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_OFFSET) Integer offset,
                                                                                 @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_LIMIT) Integer limit,
                                                                                 @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_BY) String sortBy,
                                                                                 @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_ORDER) String sortOrder) {
        return scheduleService.getAppliedForDay(offset,limit,sortBy,sortOrder);
    }

    @GetMapping(path = MappingConstants.SchedulerConstants.APPLIED_PERIOD)
    public BasePaginationApiResponse<List<ScheduleResponse>> getAllAppliedByPeriod(@RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_OFFSET) Integer offset,
                                                                                   @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_LIMIT) Integer limit,
                                                                                   @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_BY) String sortBy,
                                                                                   @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_ORDER) String sortOrder,
                                                                                   @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                                                                   @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to) {
        return scheduleService.getAppliedByPeriod(offset,limit,sortBy,sortOrder,from, to);
    }

}
