package com.VaccNow.common.pojo;

import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;


@Getter
@Setter
public class SlotPojo extends BasePojo{
    private Long branchId;
    private LocalDateTime startAt;
    private LocalDateTime endAt;

}
