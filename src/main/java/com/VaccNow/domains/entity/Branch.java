package com.VaccNow.domains.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = Branch.Columns.TABLE)
public class Branch extends Base {

	public interface Columns extends Base.Columns {
		String TABLE = "branch";
		String NAME = "name";
	}

	@Column(name = Columns.NAME)
	private String name;


}
