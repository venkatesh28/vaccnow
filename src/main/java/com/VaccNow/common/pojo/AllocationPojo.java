package com.VaccNow.common.pojo;

import com.VaccNow.domains.entity.Vaccine;
import lombok.Getter;
import lombok.Setter;
import org.dom4j.Branch;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class AllocationPojo extends BasePojo{

    private Long branchId;
    private Long vaccineId;
    private int count;


}
