package com.VaccNow.domains.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Data
@EntityListeners(AuditingEntityListener.class)
@MappedSuperclass
public class Base {

	public interface Columns {
		String ID = "id";
		String CREATED_AT = "createdAt";
		String UPDATED_AT = "updatedAt";
		String CREATED_BY = "createdBy";
		String UPDATED_BY = "updatedBy";
	}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Columns.ID, updatable = false, nullable = false)
	private Long id;

	@Column(name = Columns.CREATED_AT)
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

	@Column(name = Columns.CREATED_BY)
	@CreatedBy
	private String createdBy;

	@Column(name = Columns.UPDATED_AT)
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;

	@Column(name = Columns.UPDATED_BY)
	@LastModifiedBy
	private String updatedBy;
}
