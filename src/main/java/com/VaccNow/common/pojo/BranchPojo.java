package com.VaccNow.common.pojo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class BranchPojo extends BasePojo{

    @NotBlank(message = "Branch name can not be empty!")
    private String name;

}
