package com.VaccNow.domains.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = Vaccine.Columns.TABLE)
public class Vaccine extends Base {

	public interface Columns extends Base.Columns {
		String TABLE = "vaccine";
		String NAME = "name";
	}

	@Column(name = Columns.NAME)
	private String name;



}
