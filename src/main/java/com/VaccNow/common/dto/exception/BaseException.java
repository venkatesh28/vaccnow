package com.VaccNow.common.dto.exception;

import com.VaccNow.common.enums.ResponseCode;
import org.springframework.http.HttpStatus;

public class BaseException extends RuntimeException {
    private static final long serialVersionUID = 4735540858948700756L;
    private HttpStatus httpStatus;
    private Integer code;

    public BaseException() {
        this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public BaseException(String message, Integer code, HttpStatus httpStatus) {
        super(message);
        this.code = code;
        this.httpStatus = httpStatus;
    }

    public BaseException(ResponseCode responseCode, HttpStatus httpStatus) {
        this(responseCode.getMessage(), responseCode.getCode(), httpStatus);
    }

    public BaseException(ResponseCode responseCode) {
        this(responseCode, HttpStatus.BAD_REQUEST);
    }

    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }

    public Integer getCode() {
        return this.code;
    }
}
