package com.VaccNow.common.dto.response;

import com.VaccNow.common.enums.ResponseCode;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BasePaginationApiResponse<T> extends BaseApiResponse<T> {
    private long totalCount;
    private boolean hasNext;

    public BasePaginationApiResponse(T data, Boolean error, Integer code, String message, String detailedMessage, long totalCount, boolean hasNext) {
        super(data, error, code, message, detailedMessage);
        this.totalCount = totalCount;
        this.hasNext = hasNext;
    }

    public BasePaginationApiResponse(T data, long totalCount, boolean hasNext) {
        super(data, ResponseCode.SUCCESS);
        this.totalCount = totalCount;
        this.hasNext = hasNext;
    }

    public static <T> BasePaginationApiResponse<T> of(T data, long totalCount, boolean hasNext) {
        return new BasePaginationApiResponse(data, totalCount, hasNext);
    }

    public long getTotalCount() {
        return this.totalCount;
    }

    public boolean isHasNext() {
        return this.hasNext;
    }

    public void setTotalCount(final long totalCount) {
        this.totalCount = totalCount;
    }

    public void setHasNext(final boolean hasNext) {
        this.hasNext = hasNext;
    }

    public String toString() {
        return "BasePaginationApiResponse(totalCount=" + this.getTotalCount() + ", hasNext=" + this.isHasNext() + ")";
    }
}
