package com.VaccNow.services.impl;

import com.VaccNow.common.dto.response.AllocationResponse;
import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.mapper.AllocationMapper;
import com.VaccNow.domains.dao.AllocationDao;
import com.VaccNow.domains.entity.Allocation;
import com.VaccNow.services.AllocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AllocationServiceImpl implements AllocationService {

    @Autowired
    private AllocationDao allocationDao;

    @Override
    public BasePaginationApiResponse<List<AllocationResponse>> getAll(int offset, int limit, String sortBy, String sortOrder) {
        Page<Allocation> page = allocationDao.findAll(PageRequest.of(offset, limit, Sort.by(Sort.Direction.valueOf(sortOrder), sortBy)));
        List<AllocationResponse> responseList = getResponseList(page.getContent());
        return BasePaginationApiResponse.of(responseList, page.getTotalElements(), page.hasNext());
    }

    @Override
    public BasePaginationApiResponse<List<AllocationResponse>> getAll(int offset, int limit, String sortBy, String sortOrder, Long branchId) {
        Page<Allocation> page = allocationDao.findAllByBranchId(PageRequest
                        .of(offset, limit, Sort.by(Sort.Direction.valueOf(sortOrder), sortBy)), branchId);
        List<AllocationResponse> responseList = getResponseList(page.getContent());
        return BasePaginationApiResponse.of(responseList, page.getTotalElements(), page.hasNext());
    }

    private List<AllocationResponse> getResponseList(List<Allocation> entityList) {
        if (CollectionUtils.isEmpty(entityList)) {
            return new ArrayList<>();
        }
        return entityList.stream().map(entity -> getResponse(entity)).collect(Collectors.toList());
    }

    private AllocationResponse getResponse(Allocation entity) {
        AllocationResponse response = AllocationMapper.entityToResponse(entity);
        if (entity == null) {
            return null;
        }
        return response;
    }
}
