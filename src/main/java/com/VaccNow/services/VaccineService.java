package com.VaccNow.services;

import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.VaccineResponse;

import java.util.List;

public interface VaccineService {
    BasePaginationApiResponse<List<VaccineResponse>> getAll(int offset, int limit, String sortBy, String sortOrder);

}
