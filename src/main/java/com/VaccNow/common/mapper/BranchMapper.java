package com.VaccNow.common.mapper;

import com.VaccNow.common.dto.response.BranchResponse;
import com.VaccNow.domains.entity.Branch;

public class BranchMapper extends BaseMapper{
    public static BranchResponse entityToResponse(Branch entity) {
        if (entity == null) {
            return null;
        }
        BranchResponse response = new BranchResponse();
        response.setName(entity.getName());
        setBaseResponse(response,entity);
        return response;
    }
}
