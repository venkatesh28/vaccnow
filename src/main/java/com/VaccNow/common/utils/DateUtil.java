package com.VaccNow.common.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {

    public static final String TIMESTAMP_PATTERN = "dd-MM-yyyy HH:mm:ss";

    public DateUtil() {
    }

    public static String formatDate(Date date, String format) {
        if (date == null) {
            return null;
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.format(date);
        }
    }
}
