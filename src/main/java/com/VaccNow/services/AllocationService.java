package com.VaccNow.services;

import com.VaccNow.common.dto.response.AllocationResponse;
import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.VaccineResponse;

import java.util.List;

public interface AllocationService {
    BasePaginationApiResponse<List<AllocationResponse>> getAll(int offset, int limit, String sortBy, String sortOrder);
    BasePaginationApiResponse<List<AllocationResponse>> getAll(int offset, int limit, String sortBy, String sortOrder,Long branchId);

}
