package com.VaccNow.domains.dao;

import com.VaccNow.domains.entity.Allocation;
import com.VaccNow.domains.entity.Schedule;
import com.VaccNow.domains.entity.Vaccine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ScheduleDao extends JpaRepository<Schedule,Long> {
    Page<Schedule> findAllByIsApplied(Pageable of, boolean isApplied);
    Page<Schedule> findAllByBranchId(Pageable of, Long branchId);
    Page<Schedule> findByAppliedOnGreaterThanEqualAndAppliedOnLessThanEqual(Pageable of,LocalDateTime from, LocalDateTime to);


}
