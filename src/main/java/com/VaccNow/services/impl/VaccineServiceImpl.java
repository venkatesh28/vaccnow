package com.VaccNow.services.impl;

import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.BranchResponse;
import com.VaccNow.common.dto.response.VaccineResponse;
import com.VaccNow.common.mapper.BranchMapper;
import com.VaccNow.common.mapper.VaccineMapper;
import com.VaccNow.domains.dao.VaccineDao;
import com.VaccNow.domains.entity.Branch;
import com.VaccNow.domains.entity.Vaccine;
import com.VaccNow.services.VaccineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VaccineServiceImpl implements VaccineService {

    @Autowired
    private VaccineDao vaccineDoa;

    @Override
    public BasePaginationApiResponse<List<VaccineResponse>> getAll(int offset,
                                                                   int limit,
                                                                   String sortBy,
                                                                   String sortOrder) {
        Page<Vaccine> page = vaccineDoa.findAll(PageRequest.of(offset, limit, Sort.by(Sort.Direction.valueOf(sortOrder), sortBy)));
        List<VaccineResponse> responseList = getResponseList(page.getContent());
        return BasePaginationApiResponse.of(responseList, page.getTotalElements(), page.hasNext());
    }

    private List<VaccineResponse> getResponseList(List<Vaccine> entityList) {
        if (CollectionUtils.isEmpty(entityList)) {
            return new ArrayList<>();
        }
        return entityList.stream().map(entity -> getResponse(entity)).collect(Collectors.toList());
    }

    private VaccineResponse getResponse(Vaccine entity) {
        VaccineResponse response = VaccineMapper.entityToResponse(entity);
        if (entity == null) {
            return null;
        }
        return response;
    }
}
