package com.VaccNow.services;

import com.VaccNow.common.dto.response.BaseApiResponse;
import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.BranchResponse;
import com.VaccNow.common.dto.response.SlotResponse;

import java.util.List;

public interface SlotService {

    BasePaginationApiResponse<List<SlotResponse>> getAvailableSlots(int offset,
                                                    int limit,
                                                    String sortBy,
                                                    String sortOrder,
                                                    Long branchId);
}
