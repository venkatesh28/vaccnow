package com.VaccNow.services.impl;

import com.VaccNow.common.dto.response.BaseApiResponse;
import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.BranchResponse;
import com.VaccNow.common.mapper.BranchMapper;
import com.VaccNow.domains.dao.BranchDao;
import com.VaccNow.domains.dao.SlotDao;
import com.VaccNow.domains.entity.Branch;
import com.VaccNow.domains.entity.Slot;
import com.VaccNow.services.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BranchServiceImpl implements BranchService {

    @Autowired
    private BranchDao branchDao;

    @Override
    public BasePaginationApiResponse<List<BranchResponse>> getAll(int offset,
                                                                  int limit,
                                                                  String sortBy,
                                                                  String sortOrder) {
        Page<Branch> page = branchDao.findAll(PageRequest.of(offset, limit, Sort.by(Sort.Direction.valueOf(sortOrder), sortBy)));
        List<BranchResponse> responseList = getResponseList(page.getContent());
        return BasePaginationApiResponse.of(responseList, page.getTotalElements(), page.hasNext());
    }

    private List<BranchResponse> getResponseList(List<Branch> entityList) {
        if (CollectionUtils.isEmpty(entityList)) {
            return new ArrayList<>();
        }
        return entityList.stream().map(entity -> getResponse(entity)).collect(Collectors.toList());
    }

    private BranchResponse getResponse(Branch entity) {
        BranchResponse response = BranchMapper.entityToResponse(entity);
        if (entity == null) {
            return null;
        }
        return response;
    }
}
