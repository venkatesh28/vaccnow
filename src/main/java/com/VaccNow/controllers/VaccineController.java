package com.VaccNow.controllers;

import com.VaccNow.common.constants.MappingConstants;
import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.BranchResponse;
import com.VaccNow.common.dto.response.VaccineResponse;
import com.VaccNow.services.VaccineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = (MappingConstants.VaccineConstants.API_BASE))
public class VaccineController {

    @Autowired
    private VaccineService vaccineService;

    @GetMapping
    public BasePaginationApiResponse<List<VaccineResponse>> getAll(
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_OFFSET) Integer offset,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_LIMIT) Integer limit,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_BY) String sortBy,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_ORDER) String sortOrder) {

        return vaccineService.getAll(offset,limit,sortBy,sortOrder);

    }
}
