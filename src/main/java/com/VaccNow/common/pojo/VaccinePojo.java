package com.VaccNow.common.pojo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class VaccinePojo extends BasePojo{

    @NotBlank(message = "Vaccine name can not be empty!")
    private String name;

}
