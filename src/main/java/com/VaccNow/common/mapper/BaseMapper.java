package com.VaccNow.common.mapper;

import com.VaccNow.common.pojo.BasePojo;
import com.VaccNow.common.utils.DateUtil;
import com.VaccNow.domains.entity.Base;

public class BaseMapper {

    public static void setBaseEntity(Base entity) {
    }

    public static void setBaseResponse(BasePojo response, Base entity) {
        response.setId(entity.getId());
        response.setCreatedAt(DateUtil.formatDate(entity.getCreatedAt(), DateUtil.TIMESTAMP_PATTERN));
        response.setUpdatedAt(DateUtil.formatDate(entity.getUpdatedAt(), DateUtil.TIMESTAMP_PATTERN));
    }

}
