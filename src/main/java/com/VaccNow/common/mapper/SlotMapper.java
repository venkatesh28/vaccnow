package com.VaccNow.common.mapper;

import com.VaccNow.common.dto.response.SlotResponse;
import com.VaccNow.domains.entity.Slot;

public class SlotMapper extends BaseMapper{

    public static SlotResponse entityToResponse(Slot entity) {
        if (entity == null) {
            return null;
        }
        SlotResponse response = new SlotResponse();
        response.setBranchId(entity.getBranch().getId());
        response.setStartAt(entity.getStartAt());
        response.setEndAt(entity.getEndAt());
        setBaseResponse(response,entity);
        return response;
    }
}
