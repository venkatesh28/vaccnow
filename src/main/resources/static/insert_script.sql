insert into branch (id,name,created_at,updated_at) values (1, 'Kolkata', now(), now());
insert into branch (id,name,created_at,updated_at) values (2, 'Bengaluru', now(), now());
insert into branch (id,name,created_at,updated_at) values (3, 'Mumbai', now(), now());
insert into branch (id,name,created_at,updated_at) values (4, 'Delhi', now(), now());
insert into branch (id,name,created_at,updated_at) values (5, 'Chennai', now(), now());


insert into vaccine (id,name,created_at,updated_at) values (1,'COVAXIN',now(),now());
insert into vaccine (id,name,created_at,updated_at) values (2,'PFIZER',now(),now());
insert into vaccine (id,name,created_at,updated_at) values (3,'BHARATTEC',now(),now());


insert into slot (id, start_at, end_at, branch_id) values (1,'2021-01-18 16:36:45.946328','2021-01-18 16:21:45.946328',1);


insert into slot (id, end_at, start_at, branch_id) values (2,'2021-03-02 16:36:45.946328','2021-03-01 17:21:45.946328',1);
insert into slot (id, end_at, start_at, branch_id) values (3,'2021-03-02 16:36:45.946328','2021-03-01 12:21:45.946328',1;
insert into slot (id, end_at, start_at, branch_id) values (4,'2021-01-02 16:36:45.946328','2021-03-01 05:21:45.946328',1);
insert into slot (id, end_at, start_at, branch_id) values (5,'2021-01-02 16:36:45.946328','2021-03-02 16:21:45.946328',2);
insert into slot (id, end_at, start_at, branch_id) values (6,'2021-01-02 16:36:45.946328','2021-03-01 17:21:45.946328',2);
insert into slot (id, end_at, start_at, branch_id) values (7,'2021-01-02 16:36:45.946328','2021-03-01 05:21:45.946328',2);
insert into slot (id, end_at, start_at, branch_id) values (9,'2021-01-02 16:36:45.946328','2021-03-02 00:21:45.946328',2);
insert into slot (id, end_at, start_at, branch_id) values (10,'2021-01-02 16:36:45.946328','2021-03-01 19:21:45.946328',3);
insert into slot (id, end_at, start_at, branch_id) values (11,'2021-01-02 16:36:45.946328','2021-03-01 16:21:45.946328',3);
insert into slot (id, end_at, start_at, branch_id) values (12,'2021-01-02 16:36:45.946328','2021-03-01 17:21:45.946328',4);
insert into slot (id, end_at, start_at, branch_id) values (13,'2021-01-02 16:36:45.946328','2021-03-02 04:21:45.946328',4);
insert into slot (id, end_at, start_at, branch_id) values (14,'2021-01-02 16:36:45.946328','2021-03-02 03:21:45.946328',4);
insert into slot (id, end_at, start_at, branch_id) values (15,'2021-01-02 16:36:45.946328','2021-03-02 02:21:45.946328',5);
