package com.VaccNow.common.mapper;

import com.VaccNow.common.dto.response.AllocationResponse;
import com.VaccNow.domains.entity.Allocation;

public class AllocationMapper extends BaseMapper{

    public static AllocationResponse entityToResponse(Allocation entity) {
        if (entity == null) {
            return null;
        }
        AllocationResponse response = new AllocationResponse();
        response.setBranchId(entity.getBranch().getId());
        response.setVaccineId(entity.getVaccine().getId());
        response.setCount(entity.getCount());
        setBaseResponse(response,entity);
        return response;
    }
}
