package com.VaccNow.domains.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = Schedule.Columns.TABLE)
public class Schedule extends Base {

	public interface Columns extends Base.Columns {
		String TABLE = "schedule";
		String ALLOCATION_ID = "allocation_id";
		String SLOT_ID = "slot_id";
		String BRANCH_ID = "branch_id";
		String EMAIL = "email";
		String IS_APPLIED = "is_applied";
		String APPLIED_ON = "applied_on";
		String PAYMENT_TYPE = "payment_type";

	}

	@OneToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = Columns.ALLOCATION_ID, referencedColumnName = Columns.ID)
	private Allocation allocation;

	@OneToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = Columns.SLOT_ID, referencedColumnName = Columns.ID)
	private Slot slot;

	@OneToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = Columns.BRANCH_ID, referencedColumnName = Columns.ID)
	private Branch branch;

	@Column(name = Columns.EMAIL)
	private String email;

	@Column(name = Columns.IS_APPLIED)
	private boolean isApplied;

	@Column(name = Columns.APPLIED_ON)
	private LocalDateTime appliedOn;

	@Column(name = Columns.PAYMENT_TYPE)
	private String paymentType;

}
