package com.VaccNow.controllers;

import com.VaccNow.common.constants.MappingConstants;
import com.VaccNow.common.dto.response.BaseApiResponse;
import com.VaccNow.common.dto.response.BasePaginationApiResponse;
import com.VaccNow.common.dto.response.BranchResponse;
import com.VaccNow.common.dto.response.SlotResponse;
import com.VaccNow.services.BranchService;
import com.VaccNow.services.SlotService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = (MappingConstants.BranchConstants.API_BASE))
public class BranchController {

    @Autowired
    private BranchService branchService;

    @Autowired
    private SlotService slotService;

    @ApiOperation(value = "Get all branches.")
    @GetMapping
    public BasePaginationApiResponse<List<BranchResponse>> getAll(
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_OFFSET) Integer offset,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_LIMIT) Integer limit,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_BY) String sortBy,
            @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_ORDER) String sortOrder) {

        return branchService.getAll(offset,limit,sortBy,sortOrder);

    }

    @GetMapping(value = MappingConstants.BranchConstants.AVAILABLE_SLOTS)
    public BasePaginationApiResponse<List<SlotResponse>> getAvailableSlots(@RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_OFFSET) Integer offset,
                                                                     @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_LIMIT) Integer limit,
                                                                     @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_BY) String sortBy,
                                                                     @RequestParam(required = false, defaultValue = MappingConstants.PaginationConstants.DEFAULT_SORT_ORDER) String sortOrder,
                                                                     @PathVariable Long branchId){
        return slotService.getAvailableSlots(offset,limit,sortBy,sortOrder,branchId);
    }

}
