package com.VaccNow.common.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasePojo {
    private Long id;
    private String createdAt;
    private String updatedAt;
}
