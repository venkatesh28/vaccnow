package com.VaccNow.domains.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = Allocation.Columns.TABLE)
public class Allocation extends Base {

	public interface Columns extends Base.Columns {
		String TABLE = "allocation";
		String BRANCH_ID = "branch_id";
		String VACCINE_ID = "vaccine_id";
		String COUNT = "count";
	}

	@OneToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = Columns.BRANCH_ID, referencedColumnName = Columns.ID)
	private Branch branch;

	@OneToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = Columns.VACCINE_ID, referencedColumnName = Columns.ID)
	private Vaccine vaccine;

	@Column(name = Columns.COUNT)
	private int count;


}
