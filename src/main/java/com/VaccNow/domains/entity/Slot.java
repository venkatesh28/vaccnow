package com.VaccNow.domains.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = Slot.Columns.TABLE)
public class Slot extends Base {

	public interface Columns extends Base.Columns {
		String TABLE = "slot";
		String BRANCH_ID = "branch_id";
		String START_AT = "start_at";
		String END_AT = "end_at";
	}

	@OneToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = Columns.BRANCH_ID, referencedColumnName = Columns.ID)
	private Branch branch;

	@Column(name = Columns.START_AT)
	private LocalDateTime startAt;

	@Column(name = Columns.END_AT)
	private LocalDateTime endAt;

}
