package com.VaccNow.common.dto.response;


import com.VaccNow.common.enums.ResponseCode;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;

@JsonInclude(Include.NON_NULL)
public class BaseApiResponse<T> {
    private T data;
    private Boolean error;
    private String message;
    private Integer code;
    private String detailedMessage;
    protected List<String> fieldErrors;

    public BaseApiResponse() {
    }

    public BaseApiResponse(T data, Boolean error, Integer code, String message, String detailedMessage) {
        this.data = data;
        this.error = error;
        this.code = code;
        this.message = message;
        this.detailedMessage = detailedMessage;
    }

    public BaseApiResponse(Boolean error, Integer code, String message) {
        this((T) null, error, code, message, (String)null);
    }

    public BaseApiResponse(T data, ResponseCode responseCode) {
        this(data, false, responseCode.getCode(), responseCode.getMessage(), (String)null);
    }

    public BaseApiResponse(Boolean error, ResponseCode responseCode, String detailedMessage) {
        this((T) null, error, responseCode.getCode(), responseCode.getMessage(), detailedMessage);
    }

    public static <T> BaseApiResponse<T> data(T data) {
        return new BaseApiResponse(data, ResponseCode.SUCCESS);
    }

    public static <T> BaseApiResponse<T> dataOnly(T data) {
        return new BaseApiResponse(data, (Boolean)null, (Integer)null, (String)null, (String)null);
    }

    public static <T> BaseApiResponse<T> message(ResponseCode responseCode) {
        return new BaseApiResponse(false, responseCode, (String)null);
    }

    public static <T> BaseApiResponse<T> messageOnly(ResponseCode responseCode) {
        return new BaseApiResponse((Object)null, (Boolean)null, (Integer)null, responseCode.getMessage(), (String)null);
    }

    public static <T> BaseApiResponse<T> error(ResponseCode responseCode) {
        return new BaseApiResponse(true, responseCode, (String)null);
    }

    public static <T> BaseApiResponse<T> error(ResponseCode responseCode, String detailedMessage) {
        return new BaseApiResponse(true, responseCode, detailedMessage);
    }

    public static <T> BaseApiResponse<T> error(ResponseCode responseCode, List<String> fieldErrors) {
        BaseApiResponse<T> response = new BaseApiResponse(true, responseCode, (String)null);
        response.setFieldErrors(fieldErrors);
        return response;
    }

    public static <T> BaseApiResponse<T> unauthorized() {
        return error(ResponseCode.UNAUTHORIZED);
    }

    public static <T> BaseApiResponse<T> forbidden() {
        return error(ResponseCode.FORBIDDEN);
    }

    public void setData(final T data) {
        this.data = data;
    }

    public void setError(final Boolean error) {
        this.error = error;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public void setCode(final Integer code) {
        this.code = code;
    }

    public void setDetailedMessage(final String detailedMessage) {
        this.detailedMessage = detailedMessage;
    }

    public void setFieldErrors(final List<String> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

    public T getData() {
        return this.data;
    }

    public Boolean getError() {
        return this.error;
    }

    public String getMessage() {
        return this.message;
    }

    public Integer getCode() {
        return this.code;
    }

    public String getDetailedMessage() {
        return this.detailedMessage;
    }

    public List<String> getFieldErrors() {
        return this.fieldErrors;
    }

    public String toString() {
        return "BaseApiResponse(data=" + this.getData() + ", error=" + this.getError() + ", message=" + this.getMessage() + ", code=" + this.getCode() + ", detailedMessage=" + this.getDetailedMessage() + ", fieldErrors=" + this.getFieldErrors() + ")";
    }
}

