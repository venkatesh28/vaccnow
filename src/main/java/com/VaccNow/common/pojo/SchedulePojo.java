package com.VaccNow.common.pojo;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
public class SchedulePojo extends BasePojo{
    private Long allocationId;
    private Long slotId;
    private Long branchId;
    private String email;
    private boolean isApplied;
    private LocalDateTime appliedOn;
    private String paymentType;

}
