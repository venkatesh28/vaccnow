package com.VaccNow.common.mapper;

import com.VaccNow.common.dto.response.VaccineResponse;
import com.VaccNow.domains.entity.Vaccine;

public class VaccineMapper extends BaseMapper{

    public static VaccineResponse entityToResponse(Vaccine entity) {
        if (entity == null) {
            return null;
        }
        VaccineResponse response = new VaccineResponse();
        response.setName(entity.getName());
        setBaseResponse(response,entity);
        return response;
    }
}
